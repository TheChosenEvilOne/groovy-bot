package tceo

import com.esotericsoftware.yamlbeans.YamlReader
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import net.dv8tion.jda.api.events.GenericEvent
import net.dv8tion.jda.api.hooks.EventListener
import tceo.utils.BackgroundTaskSystem
import tceo.utils.EventSystem
import tceo.utils.Localization
import tceo.utils.Logger
import tceo.vdds.VDDS
import tceo.commands.CommandRegistery

import javax.annotation.Nonnull

import static org.codehaus.groovy.control.customizers.builder.CompilerCustomizationBuilder.withConfig

class BotMain {
    static String prefix

    static GroovyScriptEngine engine
    static JDA client
    static CommandRegistery registry
    // This is called Loc because you can't make static call operators, so I can't just import Localization and do Localization()...
    // But at least I can do this and call Loc() in the modules.
    static Localization Loc
    static Logger Log
    static EventSystem Events
    static VDDS DB

    static BackgroundTaskSystem backgroundTasks

    static File modulesFile
    static File librariesFile
    static File logsFile
    private static File configFile

    static void main(String[] args) {
        configFile = new File(args ? args[0] ?: "config.yaml" : "config.yaml")
        if (!configFile.file) {
            println "config file is not a file"
            return
        }

        def reader = new YamlReader(configFile.newReader())
        def config = reader.read() as HashMap

        modulesFile = new File(config.get("moduleFolder") as String)
        librariesFile = new File(config.get("libraryFolder") as String)
        logsFile = new File(config.get("logFolder") as String)
        Log = new Logger()
        Log("Logger initialized.")
        Loc = new Localization(config.get("localization") as String)
        Log("Localization initialized.")
        Events = new EventSystem()
        Log("Event system initialized.")
        DB = new VDDS(config.get("dataFolder") as String)
        Log("VDDS initialized.")
        backgroundTasks = new BackgroundTaskSystem()
        Log("Backgorund task system initialized.")

        librariesFile.mkdirs()
        modulesFile.mkdirs()

        // CommandRegistery is misspelled, fix at some point. thanks.
        registry = new CommandRegistery()
        Log("Command registry initialized.")
        client = new JDABuilder(config.get("token") as String).addEventListeners(new EventListener() {
            @Override
            void onEvent(@Nonnull GenericEvent event) {
                Events << [event.class.simpleName, event]
            }
        }).build()
        Log("Client created.")

        prefix = config.get "prefix"
        Log("Prefix set to $prefix.")
        SyntaxArgs.registerArgTypes()
        Log("ESSNT arg types registered.")
        runScripts()
        Log("READY!")
    }

    static void runScripts() {
        Events.paused = true
        Events.clear()
        Loc.loadLocalizations()
        EventHandlers.registerEventHandlers()
        registry.clear()

        // Unload all classes.
        if(engine) {
            for (Class<?> groovyClass : engine.groovyClassLoader.getLoadedClasses()) {
                GroovySystem.getMetaClassRegistry().removeMetaClass(groovyClass)
            }
        }

        engine = new GroovyScriptEngine(modulesFile.toURI().toURL())
        withConfig(engine.config) {
            imports {
                normal "tceo.utils.GeneralUtils"
                star "tceo.commands"
                staticMember "tceo.BotMain", "Log"
                staticMember "tceo.BotMain", "Loc"
                staticMember "tceo.BotMain", "Events"
            }
        }

        for (File f:librariesFile.listFiles()) {
            Log("Loading library $f.name")
            engine.getGroovyClassLoader().addURL(f.toURI().toURL())
        }

        for(File f:modulesFile.listFiles()){
            if(f.isDirectory()) continue

            try {
                Log("Running script $f.name")
                engine.run(f.name, "")
            } catch (Exception e) {
                e.printStackTrace()
            }
        }

        backgroundTasks.start()
        Events.paused = false
    }
}
