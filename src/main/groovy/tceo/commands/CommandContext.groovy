package tceo.commands

import net.dv8tion.jda.api.entities.Message

class CommandContext {
    Message message
    String content
    List args

    CommandContext(Message message, String content, List args) {
        this.message = message
        this.content = content
        this.args = args
    }

    def reply(def reply) {
        message.channel.sendMessage("$reply").queue()
    }

    def getUser() {
        message.author
    }

    def getMember() {
        message.member
    }

    def getGuild() {
        message.guild
    }

    def getMention() {
        message.author.asMention
    }
}
