package tceo.commands

class RunnableCommand implements Runnable {
    def ctx
    def runner

    RunnableCommand(Closure runner, CommandContext ctx) {
        this.ctx = ctx
        this.runner = runner
    }

    @Override
    void run() {
        try {
            runner ctx
        } catch (Exception e) {
            ctx.reply("An exception occurred: `$e`")
            e.printStackTrace()
        }
    }
}
