package tceo

import net.dv8tion.jda.api.events.GenericEvent
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import tceo.commands.Command
import tceo.utils.GeneralUtils
import tceo.commands.CommandContext

import static tceo.BotMain.Events

class EventHandlers {
    static void registerEventHandlers() {
        Events + [MessageReceivedEvent.simpleName, { MessageReceivedEvent event ->
            def message = event.message
            BotMain.Log("$message.guild.name #$message.channel.name: $message.author.name: $message.contentRaw")
            if (!event.author.bot && message.contentRaw.startsWith(BotMain.prefix)) {
                String commandName = message.contentRaw.split(" ").first().replace(BotMain.prefix, "")
                Command command = BotMain.registry.getCommandByName(commandName)
                if (command) {
                    def commPrefix = "$BotMain.prefix$commandName"
                    if (message.contentRaw.indexOf(" ") == "$BotMain.prefix$commandName".length()) commPrefix += " "

                    def content = message.contentRaw.replace(commPrefix, "")
                    def snode = command.syntax.validate(content as String)
                    if(snode) {
                        def exp = BotMain.registry.runCommand(command, new CommandContext(message, content, snode.convert(content as String)))
                        if (exp) {
                            message.channel.sendMessage(exp.toString()).queue()
                            exp.printStackTrace()
                        }
                    } else
                        message.channel.sendMessage("Invalid syntax for $commandName").queue()
                } else {
                    GeneralUtils.deleteMessageAfter (message.channel.sendMessage("Unknown command `${commandName.replace("`", "˛")}`").complete(), 10000)
                    message.delete().queue()
                }
            }
        }]
    }
}
