package tceo.vdds

import com.esotericsoftware.yamlbeans.YamlReader

class VDDS {
    private Map<String, Database> databases

    public VDDS(String path) {
        DataType.registerDefaults()
        File mf = new File("$path/meta.yaml")
        if(!mf.exists())
            throw new FileNotFoundException()
        def meta = new YamlReader(mf.newReader()).read()
        databases = [:]
        meta.forEach {k, v ->
            def db = new Database(k)
            def fields = [:]
            v.fields.each { name, type -> 
                fields[name] = DataType.getDataType(type)
            }
            println fields
            db.load("$path/$k", fields)
            databases[k] = db
        }
    }
}