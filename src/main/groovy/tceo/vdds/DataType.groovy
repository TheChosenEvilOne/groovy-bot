package tceo.vdds

class DataType {
    private static Map<String, DataType> dataTypes = [:]

    String name
    Closure reader
    Closure writer

    DataType(String name, Closure reader, Closure writer) {
        this.name = name
        this.reader = reader
        this.writer = writer
    }
    
    static boolean register(DataType dt) {
        if(dataTypes[dt.name]) return false
        dataTypes[dt.name] = dt
        return true
    }

    static DataType getDataType(String name) {
        if(!dataTypes[name]) return null
        return dataTypes[name]
    }

    static void registerDefaults() {
        register(new DataType("String",
            {DataInputStream is -> 
                return is.readUTF()
            }, {String s, DataOutputStream os -> 
                os.writeUTF(s)
                return os.size()
            }))

        register(new DataType("Integer",
            {DataInputStream is ->
                return is.readInt()
            }, {Integer i, DataOutputStream os -> 
                os.writeInt(i)
                return os.size()
            }))

        register(new DataType("Long",
            {DataInputStream is ->
                return is.readLong()
            }, {Long l, DataOutputStream os -> 
                os.writeLong(l)
                return os.size()
            }))
    }
}