package tceo.utils

import org.codehaus.groovy.runtime.StackTraceUtils
import tceo.BotMain

import java.time.LocalDateTime
import java.time.LocalTime

class Logger {
    def logFile
    def lastDay

    enum Severity {
        ERR("31m"),
        WARN("33m"),
        INFO("34m"),
        DBG("35m")

        public String color
        Severity(String color) {
            this.color = color
        }
    }

    Logger() {
        createLogFile()
        lastDay = LocalDateTime.now().dayOfMonth
    }

    def createLogFile() {
        def now = LocalDateTime.now()
        logFile = new File("$BotMain.logsFile/$now.year/$now.monthValue/${now.dayOfMonth}.log")
        if(!logFile.parentFile.exists())
            logFile.parentFile.mkdirs()
        logFile.createNewFile()
    }

    def call(String message, Severity severity = Severity.INFO) {
        // This feels really inefficient, but I HOPE it does not get called that often.
        if(lastDay != LocalDateTime.now().dayOfMonth)
            createLogFile()
        def stackTrace = StackTraceUtils.sanitize(new Exception()).stackTrace
        def file = (stackTrace[2].fileName?:stackTrace[3].fileName).replace(".groovy","")
        println "[${LocalTime.now()}] \u001B[$severity.color$severity: $message\u001B[0m @ \u001B[32m$file\u001B[0m"
        logFile.append("[${LocalTime.now()}] $severity: $message @ $file\n")
    }
}
