package tceo.utils

import com.esotericsoftware.yamlbeans.YamlReader
import org.codehaus.groovy.runtime.StackTraceUtils

import java.util.regex.Pattern

class Localization {
    Map localizations
    String locale
    Random random = new Random()

    Localization(String locale) {
        this.locale = locale
        loadLocalizations()
    }

    void loadLocalizations() {
        localizations = new YamlReader(new File("${locale}.yaml").newReader()).read() as HashMap
    }

    String call(String key, def... values) {
        def module = StackTraceUtils.sanitize(new Throwable()).stackTrace[1].fileName.replace(".groovy", "")
        println module
        Map moduleLocs = localizations.get(module) as Map
        def loc = moduleLocs.get(key)
        if (loc instanceof List) {
            loc =  loc.get(random.nextInt(loc.size()))
        }
        assert loc instanceof String
        for(int i = 0; i < values.size(); i++){
            loc = loc.replaceAll(Pattern.quote("&[" + i + "]"), values[i] as String)
        }
        loc
    }
}
