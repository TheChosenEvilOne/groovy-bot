package tceo.utils

import java.time.Duration

import static tceo.BotMain.Log

class BackgroundTaskSystem {
    private static final Duration timeout = Duration.ofSeconds(2)

    private Thread workerThread, timerThread
    private Map<BackgroundTask, Long> timedTasks
    private List<BackgroundTask> queuedTasks
    private List<BackgroundTask> addQueue
    private BackgroundTask runningTask
    private long workTimeoutAt

    BackgroundTaskSystem() {
        reset()
    }

    void reset() {
        timedTasks = new HashMap<>()
        queuedTasks = new ArrayList<>()
        addQueue = new ArrayList<>()
        createWorkerThread()
        timerThread = new Thread({
            while (!Thread.interrupted()) {
                long currTime = System.currentTimeMillis()
                if(currTime > workTimeoutAt) {
                    Log("Background Task Worker Thread Timeout, restarting.", Logger.Severity.ERR)
                    createWorkerThread()
                    workerThread.start()
                    queuedTasks.clear()
                    workTimeoutAt = Long.MAX_VALUE
                    if(++runningTask.strikes >= 3) {
                        timedTasks.remove(runningTask)
                        Log("Removed background task $runningTask for getting $runningTask.strikes strikes.", Logger.Severity.DBG)
                    } else {
                        Log("Striked background task $runningTask, now has $runningTask.strikes strike(s).", Logger.Severity.DBG)
                    }
                }

                if(addQueue)
                    timedTasks.put(addQueue.remove(0), 0)

                for (BackgroundTask task in timedTasks.keySet()) {
                    if (timedTasks[task] < currTime) {
                        queuedTasks.add(task)
                        timedTasks[task] = currTime + task.time.toMillis()
                    }
                }
            }
        })
    }

    private void createWorkerThread() {
        if(workerThread)
            workerThread.stop()

        workerThread = new Thread({
            while (!Thread.interrupted()) {
                workTimeoutAt = System.currentTimeMillis() + timeout.toMillis()
                if (queuedTasks) {
                    runningTask = queuedTasks.first()
                    queuedTasks.remove(0)
                    runningTask.runner.run()
                }
            }
        })
    }

    void start() {
        workTimeoutAt = System.currentTimeMillis() + timeout.toMillis()
        workerThread.start()
        timerThread.start()
    }

    void addTask(BackgroundTask task) {
        addQueue.add(task)
    }
}
