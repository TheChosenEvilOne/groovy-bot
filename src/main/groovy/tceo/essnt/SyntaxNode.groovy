package tceo.essnt

class SyntaxNode {
    static enum NodeType {
        OPTIONAL,
        REQUIRED,
        PARENT
    }

    String name
    NodeType nodeType
    SyntaxArgType type
    Object[] typeArgs
    SyntaxNode[] children = []
    int longestPath
    SyntaxNode parent

    SyntaxNode(String name, NodeType nodeType, SyntaxArgType argType, Object[] argTypeArgs) {
        this.name = name
        this.nodeType = nodeType
        this.type = argType
        this.typeArgs = argTypeArgs
    }

    SyntaxNode(String name, NodeType nodeType) {
        this.name = name
        this.nodeType = nodeType
    }

    boolean addChild(SyntaxNode node) {
        // Don't try to add a PARENT node under a node.
        if(node.nodeType == NodeType.PARENT) return false
        // Don't make a loop
        if(node.parent.is(this) || parent.is(node)) return false

        children += node
        node.parent = this
        if(parent) {
            longestPath += 1
            pathSizeIncrease(longestPath)
        }
        return true
    }

    void pathSizeIncrease(int pathLength) {
        if(!parent) {
            if(pathLength <= longestPath) return
            longestPath = pathLength
        } else {
            if(pathLength + 1 <= longestPath) return 
            longestPath = pathLength + 1
            parent.pathSizeIncrease(longestPath)
        }
    }

    SyntaxNode validate(String toValidate) {
        if(nodeType == NodeType.PARENT) {
            String[] toValidateCut = (toValidate =~ /[^\s"']+|"([^"]*)"|'([^']*)'/).findAll()*.first()

            if(!children) return this
            if(!toValidateCut.length) return null
            // Too many arguments, it can't possibly be.
            if(longestPath < toValidateCut.length) return null
            
            for(SyntaxNode child : children) {
                // -1 on toValidateCut.length as the child itself takes an argument.
                if (child.longestPath < toValidateCut.length - 1) continue
                def valid = child.validate(toValidateCut)
                if (valid) return valid
            }
            return null
        }
        throw new Exception("validate should not be called on non-parent SyntaxNodes")
    }

    SyntaxNode validate(String[] toValidate) {
        def valid = typeArgs.size()
            ? type.validate(toValidate[0], typeArgs)
            : type.validate(toValidate[0])
        if (valid) {
            if(toValidate.length == 1) {
                if(!children) return this

                for(SyntaxNode child : children) {
                    if(child.nodeType == NodeType.OPTIONAL) return this
                }
                
                return null
            }

            if (!children) return this
            toValidate = toValidate[1..-1]
            
            for(SyntaxNode child : children) {
                // -1 on toValidate.length as the child itself takes an argument.
                if(child.longestPath >= toValidate.length - 1) return child.validate(toValidate)
            }
        }
        return null
    }

    // convert assumes validate has executed successfully
    def convert(String toConvert) {
        if (nodeType == NodeType.PARENT) return []
        String[] toConvertCut = (toConvert =~ /[^\s"']+|"([^"]*)"|'([^']*)'/).findAll()*.first()
        if (parent.nodeType == NodeType.PARENT) return [type.convert(toConvertCut[0])]
        return [type.convert(toConvertCut[0])] + parent.convert(toConvertCut[1..-1])
    }

    def convert(List toConvert) {
        if (parent.nodeType == NodeType.PARENT) return [type.convert(toConvert[0])]
        return [type.convert(toConvert[0])] + parent.convert(toConvert[1..-1])
    }
}
