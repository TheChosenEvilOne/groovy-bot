package tceo.essnt

class SyntaxArgType {
    private static HashMap<String, SyntaxArgType> argTypes = new HashMap<>()

    String name
    Class type
    Closure<Boolean> validate
    Closure convert

    SyntaxArgType(String name, Class type, Closure<Boolean> validate, Closure convert) {
        this.name = name
        this.type = type
        this.validate = validate
        this.convert = convert
    }

    static Boolean register(SyntaxArgType argType) {
        if(argTypes.containsKey(argType.name)) return false

        argTypes.put(argType.name, argType)
        return true
    }

    static SyntaxArgType getArgType(String name) {
        if(argTypes.containsKey(name)) return argTypes.get(name)
        return null
    }

    static void clearArgTypes() {
        argTypes.clear()
    }
}