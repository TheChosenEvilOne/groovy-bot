package tceo.essnt

class SyntaxParser {
    /** How ESSNT (Extremely Stupid Syntax Node Tree-er) works:
      *  The templates are simply strings that work as follows:
      * <required argument> [optional argument]
      *  Each argument is defined as follows:
      * ArgType:Argument name (example: <User:User to kick>)
      *  When a command has a branching tree of arguments | can be used:
      * <String:example> | [Integer:example]
      *  The syntax tree for the last example would be the following:
      *                 Parent Node
      *                 /          \
      *        Branch Parent    Branch Parent
      *   (Required, String)    (Optional, Integer)
      *  
      *  Note: The first branch is always checked first. 
      *
      *  When traversing the tree, the system asks the node if the argument is correct.
      *  If there is no valid branch to traverse the arguments will be considered illegal.
      *  Do note that the optional arguments must also be valid.
      *  
      *  For arguments with spaces quotes ("") must be used unless the argument is the only one or last.
      *  This also instantly validates it for string nodes.
      *   Other types of arguments the node must check if it is valid.
      *  Argument types will be defined in SyntaxTypes. Argument types are simply the name of
      *  the type and validation rules. All argument types will get the argument as a string.
      *  
      *  Argument types also support having special arguments for more specialized control.
      *  Let's say you want to have sub commands for your command, you can create a new enum for
      *   the sub commands:
      * enum commandSubs { Foo, Bar, Baz }
      *   now that you have this enum, you can specify that the first argument has to be one of those
      * <Enum(commandSubs):Sub command>
      *   what this does is tells the Enum ArgType that the enum in question is commandSubs
      *  This ArgType argument system can be used to limit Strings or Integers for example.
      *  What the arguments for ArgTypes do is decided by the ArgType.
      *  SyntaxNode holds the arguments for the ArgType so ArgTypes don't have to be instanced specially
      *   for every single SyntaxNode that requires special arguments.
      *
      *  The syntax tree generation works like follows:
      *   Firstly the parent node is generated, this node will do nothing but stand as an entry point.
      *   The syntax template gets split at | and resulting strings are now processed syntax node "lines"
      *    Parent | -- < Arg > -- [ Arg ] etc
      *   Now it is checked that everything falls inside <> or [], if everything does not fall inside
      *    the parser should throw an error.
      *   The ArgType is now collected by splitting at : and it's arguments if they exist.
      *    ArgType arguments are stored as an array on the SyntaxNode it belongs to and are passed to
      *    the ArgType when validating a syntax.
      *   When adding children to a SyntaxNode, the parents will all have their children count increased.
      *    This is done to make validating slightly faster as it is not required to walk the entire
      *    path to check if there are enough children on said path.
      *  
      *  When validating arguments the arguments are split according to quotes and spaces.
      *   example: ' foo "bar" "baz quz" ' would become ["foo", "bar", "baz quz"]
      *  Then a path is checked that has enough children to be considered.
      *  When a SyntaxNode is reached the split argument is given to ArgType with it's arguments from
      *   the node which then decides if the argument is valid, if it is valid the system will continue.
      *  Once the traversing is complete and the arguments are considered valid the system will walk backwards
      *   and convert the arguments to the type according to ArgType.
      *   This is done so the arguments don't get converted unless the syntax is correct.
      *  
      *  ArgTypes have a registry so they can be added as required on runtime.
      *   When getting an ArgType the registry must be provided with the name for the ArgType.
      *  ArgTypes have two methods: boolean validate(String argument) and Object convert(String argument).
      *   validate will return true if argument is valid and convert will return the object from the argument.
      *   Note that convert is allowed to catastrophically fail if it is called without the code checking validate first,
      *   this is invalid behaviour and as such has unforeseen consequences.
      */
    static SyntaxNode createSyntaxTree(String syntaxTemplate) {
      SyntaxNode parent = new SyntaxNode("Parent", SyntaxNode.NodeType.PARENT)
      if(!syntaxTemplate) return parent
      String[] branches = syntaxTemplate.tokenize("|")
      for(def i = 0 ; i < branches.length ; i++) {
        String[] nodeTemplates = (branches[i].trim() =~ /(\<.+?\>)|(\[.+?\])/).findAll()*.first()
        if(!nodeTemplates) continue
        def lastNode = parent
        for (def l = 0 ; l < nodeTemplates.length ; l++) {
          SyntaxNode.NodeType type
          if (nodeTemplates[l] =~ /^\<|\>/)
            type = SyntaxNode.NodeType.REQUIRED
          else if (nodeTemplates[l] =~ /^\[|\]/)
            type = SyntaxNode.NodeType.OPTIONAL
          else break

          // Probably needs a better rule so it will not split if the name has a : in it
          def typeAndName = nodeTemplates[l][1..-2].split(":(?!.*:)")
          // You need to have both type and name, if you don't want the name you can just leave it empty.
          if(typeAndName.length < 2) break
          def m = (typeAndName[0] =~ /(\(.+?\))/)
          def args = (m ? m.findAll()*.last() : []).find() ?: ""
          Object[] argTypeArgs = args.size() ? args[1..-2].split(",") : []
          def argType = SyntaxArgType.getArgType(typeAndName[0][0..-(args.length()+1)])
          if(!argType) break

          def name = typeAndName[1]
          def node = new SyntaxNode(name, type, argType, argTypeArgs?:[])
          lastNode.addChild(node)
          lastNode = node
        }
      }
      // Special sauce handling because I can't be arsed to fix sleepy TCEO code.
      if(parent.children.length && !parent.longestPath) parent.longestPath = 1
      return parent
    }
}
