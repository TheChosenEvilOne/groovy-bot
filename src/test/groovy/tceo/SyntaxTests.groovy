package tceo

import org.junit.Test
import tceo.essnt.SyntaxArgType
import tceo.essnt.SyntaxParser

class SyntaxTests {
    @Test
    void SimpleValidate() {
        SyntaxArgType.clearArgTypes()
        SyntaxArgType.register(
                new SyntaxArgType("String", String.class,
                { String toValidate, Object[] args ->
                    return true
                },
                { String toConvert ->
                    return toConvert as String
                }
        ))

        def node = SyntaxParser.createSyntaxTree("<String:argument 1> [String:argument 2]")

        assert node.validate("a")
        assert node.validate("a b")
        assert !node.validate("a b c")
        assert node.validate("a b").convert("a b") == ["a", "b"]
        assert node.validate("a b").convert("a b")[0] instanceof String
    }

    @Test
    void ComplexValidate() {
        SyntaxArgType.clearArgTypes()
        SyntaxArgType.register(
                new SyntaxArgType("String", String.class,
                        { String toValidate, Object[] args ->
                            return true
                        },
                        { String toConvert ->
                            return toConvert as String
                        }
                )
        )
        SyntaxArgType.register(
                new SyntaxArgType("Integer", Integer.class,
                    { String toValidate, Object[] args ->
                        return toValidate.isInteger()
                        },
                        {String toConvert ->
                            return toConvert as Integer
                        }
                )
        )

        def node = SyntaxParser.createSyntaxTree("<String:argument 1> [Integer:argument 2] <String(argument):argument 3>")

        assert node.validate("a")
        assert !node.validate("a 1")
        assert !node.validate("a b")
        assert node.validate("a 1 b")
        assert !node.validate("a 1 b c")
        assert node.validate("a 1 b").convert("a 1 b") == ["a", 1, "b"]
        assert node.validate("a 1 b").convert("a 1 b")[1] instanceof Integer
    }
}
