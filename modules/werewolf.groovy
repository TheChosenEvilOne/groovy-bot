import java.time.Duration

def PHASE
def ROLES = [:]
def MAIN_ROLES = [:]
def ALL_PLAYERS = []
def JOIN_TIME_LIMIT = Duration.ofHours(1)
def TIMERS = [:]
def MIN_PLAYERS
def MAX_PLAYERS

ROLES["person"] = []

def reset() {
    PHASE = null
    ALL_PLAYERS.clear()
    ROLES.clear()
    ROLES["person"] = []
    MAIN_ROLES.clear()
    Events << ["werewolf_reset"]
}

new Command(["join", "j"] as String[], "", "",
        { CommandContext ctx ->
            if (!PHASE) {
                ROLES["person"] += ctx.user
                MAIN_ROLES[ctx.user] = "person"
                ALL_PLAYERS += ctx.user
                PHASE = "join"
                Events << ["werewolf_player_join", ctx.user]
                Events << ["werewolf_new_game"]
                ctx.reply(Loc("new_game", ctx.mention))
                TIMERS["join"] = new Timer().schedule({ ->
                    ctx.reply("PING! ${def s = ""; ALL_PLAYERS.each { p -> s += "$p.asMention " }; s}")
                    ctx.reply(Loc("game_idle_cancel"))
                    reset()
                }, JOIN_TIME_LIMIT.toMillis())
            } else if (ctx.user in ALL_PLAYERS) {
                ctx.reply(Loc("you_already_playing"))
                return
            } else if (PHASE != "join") {
                ctx.reply(Loc("game_already_running"))
                return
            } else {
                ALL_PLAYERS += ctx.user
                ROLES["person"] += ctx.user
                MAIN_ROLES[ctx.user] = "person"
                ctx.reply(Loc("player_join", ctx.mention, ALL_PLAYERS.size()))
            }
        })

new Command(["start"] as String[], "", "",
        { CommandContext ctx ->
            def villagers = ALL_PLAYERS
            if (!PHASE) {
                ctx.reply(Loc("no_game_running"))
                return
            } else if (PHASE != "join") {
                ctx.reply(Loc("werewolf_already_running"))
            }
        })