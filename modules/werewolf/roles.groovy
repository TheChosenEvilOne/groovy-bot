import Role

class Villager extends Role {
    Villager() {
        super("Villager")
        description = "You are the villager, your objective is to lynch the wolves."
    }
}
new Villager()

class Wolf extends Role {
    Wolf() {
        super("Wolf")
        canKill = true
        description = "You are the wolf, your objective is to kill the villagers."
    }
}
new Wolf()

class Seer extends Role {
    Seer() {
        super("Seer")
        canSee = true
        description = "You are the seer, you can use the command 'see' at night to reveal the true identity of a person"
    }
}
new Seer()