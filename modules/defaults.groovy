new Command(["reload"] as String[], "",
    "Reloads the modules for the bot.",
    { CommandContext ctx ->
        tceo.BotMain.runScripts()
        ctx.reply(Loc("reload_modules"))
    } as Closure<CommandContext>)

new Command(["help"] as String[], "",
    "HELP MAINT!!",
    { CommandContext ctx ->
        ctx.reply(Loc("help"))
    } as Closure<CommandContext>)

// Due to the nature of this command it DOES not have a syntax, this needs changes to ESSNT itself to fix.
new Command(["eval"] as String[], "",
    "Evaluates message.",
    { CommandContext ctx ->
        if(!(ctx.guild.getRoleById(404528320781352960) in ctx.message.member.getRoles()))
            ctx.reply(Loc("tceo_lazy"))
        else {
            try {
                ctx.reply(Eval.me(ctx.content) as String)
            } catch (Exception e) {
                ctx.reply(e as String)
            }
        }
    } as Closure<CommandContext>)

new Command(["unloadModule"] as String[], "<String:Module>",
    "Unloads commands from specified module.",
    { CommandContext ctx ->
        ctx.reply("Unloading commands from ${ctx.args[0]}.")
    } as Closure<CommandContext>)

new Command(["stateLaws"] as String[], "",
    "States this bot's laws.",
    { CommandContext ctx ->
        def laws = [Loc("law 1"), Loc("law 2"), Loc("law 3")]
        if(Math.random() < 0.1)
            laws = [Loc("law 0")] + laws
        ctx.reply(laws.join("\n"))
    } as Closure<CommandContext>)

new Command(["ping"] as String[], "",
    "PONG!!",
    { CommandContext ctx ->
        ctx.reply(Loc("ping", ctx.user.asMention))
    } as Closure<CommandContext>)

new Command(["plus"] as String[], "<Integer:number 1> <Integer:number 2>",
    "addition",
    { CommandContext ctx ->
        ctx.reply("${ctx.args[0]} + ${ctx.args[1]} = ${ctx.args[0] + ctx.args[1]}")
    } as Closure<CommandContext>)